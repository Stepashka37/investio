package ru.yandex.dimax.javatemplate.IT;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.UserInfoDto;
import ru.yandex.dimax.javatemplate.model.Gender;
import ru.yandex.dimax.javatemplate.repository.UserRepository;
import ru.yandex.dimax.javatemplate.serivce.UserService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceIT {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testUserService() {
        // Given
        RegisterUserDto registerUserDto = RegisterUserDto.builder()
                .name("testName")
                .email("testEmail@email.com")
                .age(20)
                .gender(Gender.MALE)
                .password("password")
                .build();

        // When
        UserInfoDto userDto = userService.createUser(registerUserDto);

        // Then
        assertNotNull(userDto);
        assertEquals(userDto.getId(), userRepository.findById(userDto.getId()).get().getId());
    }
}