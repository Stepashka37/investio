CREATE TABLE user_share(
    user_id UUID REFERENCES users(id),
    share_id UUID REFERENCES shares(id),
    PRIMARY KEY (user_id, share_id)
);