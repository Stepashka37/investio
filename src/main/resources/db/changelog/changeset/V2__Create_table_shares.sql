CREATE TABLE shares(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    isin VARCHAR(40) NOT NULL,
    current_price NUMERIC NOT NULL,
    market VARCHAR NOT NULL,
    dividends NUMERIC NOT NULL
);