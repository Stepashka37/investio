package ru.yandex.dimax.javatemplate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Objects;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ShareDto {

    private String name;
    private String isin;
    private Integer amount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShareDto shareDto = (ShareDto) o;
        return Objects.equals(name, shareDto.name) && Objects.equals(isin, shareDto.isin) && Objects.equals(amount, shareDto.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, isin, amount);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}
