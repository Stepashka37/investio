package ru.yandex.dimax.javatemplate.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.yandex.dimax.javatemplate.aop.agecheck.AgeGreaterThan16;
import ru.yandex.dimax.javatemplate.model.Gender;

import javax.validation.constraints.*;
import java.util.Objects;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RegisterUserDto {

    @Size(max = 50)
    @NotBlank
    private String name;

    @Size(max = 40)
    @Email
    @NotBlank
    private String email;

    @NotNull
    private Gender gender;

    @NotNull
    @AgeGreaterThan16
    private Integer age;

    @Size(max = 16)
    @NotBlank
    private String password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterUserDto that = (RegisterUserDto) o;
        return Objects.equals(name, that.name) && Objects.equals(email, that.email) && gender == that.gender && Objects.equals(age, that.age) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email, gender, age, password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

