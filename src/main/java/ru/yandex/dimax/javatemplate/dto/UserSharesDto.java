package ru.yandex.dimax.javatemplate.dto;

import lombok.*;
import ru.yandex.dimax.javatemplate.model.Gender;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class UserSharesDto {
    private UUID id;
    private String email;
    private List<ShareDto> userShares;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSharesDto that = (UserSharesDto) o;
        return Objects.equals(id, that.id) && Objects.equals(email, that.email) && Objects.equals(userShares, that.userShares);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, userShares);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ShareDto> getUserShares() {
        return userShares;
    }

    public void setUserShares(List<ShareDto> userShares) {
        this.userShares = userShares;
    }
}
