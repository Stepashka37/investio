package ru.yandex.dimax.javatemplate.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.UserInfoDto;
import ru.yandex.dimax.javatemplate.dto.UserSharesDto;
import ru.yandex.dimax.javatemplate.serivce.UserService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<UserInfoDto> getUserById(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserInfoDto> deleteUser(@PathVariable UUID id) {
        return ResponseEntity.ok(userService.deleteUser(id));
    }

    @PostMapping
    public ResponseEntity<UserInfoDto> createUser(@Valid @RequestBody RegisterUserDto userDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(userDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserInfoDto> updateUser(@PathVariable UUID id, @RequestBody @Validated UserInfoDto userDto) {
        return ResponseEntity.ok(userService.updateUser(userDto));
    }

    @PostMapping("/{id}/shares")
    public ResponseEntity<UserSharesDto> buyShare(@PathVariable UUID id, @RequestParam String shareIsin, @RequestParam Integer shareNumber) {
        return ResponseEntity.ok(userService.buyShare(id, shareIsin, shareNumber));
    }
}
