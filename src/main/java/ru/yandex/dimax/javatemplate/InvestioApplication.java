package ru.yandex.dimax.javatemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestioApplication {

    public static void main(String[] args) {
        SpringApplication.run(InvestioApplication.class, args);
    }

}
