package ru.yandex.dimax.javatemplate.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.yandex.dimax.javatemplate.dto.ShareDto;
import ru.yandex.dimax.javatemplate.model.Share;

@Mapper(componentModel = "spring")
public interface ContextMapper {

    //@Mapping(target = "name", source = "name")
    //@Mapping(target = "isin", source = "isin")
    ShareDto toShareDto(Share share);
}
