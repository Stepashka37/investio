package ru.yandex.dimax.javatemplate.mapper;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.ShareDto;
import ru.yandex.dimax.javatemplate.dto.UserInfoDto;
import ru.yandex.dimax.javatemplate.dto.UserSharesDto;
import ru.yandex.dimax.javatemplate.model.Gender;
import ru.yandex.dimax.javatemplate.model.User;
import ru.yandex.dimax.javatemplate.model.UserShare;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "gender", source = "gender", qualifiedByName = "getGenderValue")
    @Mapping(target = "age", source = "age")
    UserInfoDto toUserInfoDto(User user);

    @Mapping(target = "id", source = "id")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "userShares", source = "userShares", qualifiedByName = "mapToShareDtoSet")
    UserSharesDto toUserSharesDto(User user);

    @Mapping(target = "id", ignore = true)
    User toEntity(RegisterUserDto registerUserDto);

    @Named("getGenderValue")
    default String getGenderValue(Gender gender) {
        return gender.name();
    }

    @Named("mapToShareDtoSet")
    default List<ShareDto> mapToShareDtoSet(Set<UserShare> userShares) {
        List<ShareDto> shares = userShares.stream()
                .map(userShare -> {
                    ShareDto shareDto = Mappers.getMapper(ContextMapper.class).toShareDto(userShare.getShare());
                    shareDto.setAmount(userShare.getAmount());
                    return shareDto;
                })
                .collect(Collectors.toList());
        return shares;
    }

}
