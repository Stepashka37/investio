package ru.yandex.dimax.javatemplate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yandex.dimax.javatemplate.model.Share;


import java.util.Optional;
import java.util.UUID;

public interface ShareRepository extends JpaRepository<Share, UUID> {

    Optional<Share> findByIsin(String isin);
}
