package ru.yandex.dimax.javatemplate.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class UserSharePK implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID userId;

    private UUID shareId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserSharePK that = (UserSharePK) o;
        return Objects.equals(userId, that.userId) && Objects.equals(shareId, that.shareId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, shareId);
    }
}
