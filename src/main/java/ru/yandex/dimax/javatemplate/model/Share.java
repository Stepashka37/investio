package ru.yandex.dimax.javatemplate.model;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "shares")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Getter
@Setter
public class Share {

    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO
    )
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, unique = true)
    private String isin;

    @Column(nullable = false)
    private BigDecimal currentPrice;

    @Column(nullable = false)
    private String market;

    @Column(nullable = false)
    private BigDecimal dividends;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Share share = (Share) o;
        return Objects.equals(id, share.id) && Objects.equals(name, share.name) && Objects.equals(isin, share.isin) && Objects.equals(currentPrice, share.currentPrice) && Objects.equals(market, share.market) && Objects.equals(dividends, share.dividends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, isin, currentPrice, market, dividends);
    }
}
