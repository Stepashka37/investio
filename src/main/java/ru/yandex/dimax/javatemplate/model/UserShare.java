package ru.yandex.dimax.javatemplate.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class UserShare implements Serializable {

    private static final long serialVersionUID = 1L;

    public UserShare(User user, Share share, Integer amount) {
        this.id = new UserSharePK(user.getId(), share.getId());
        this.share = share;
        this.user = user;
        this.amount = amount;
    }

    @EmbeddedId
    private UserSharePK id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("shareId")
    private Share share;

    private Integer amount;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserShare userShare = (UserShare) o;
        return Objects.equals(id, userShare.id) && Objects.equals(user, userShare.user) && Objects.equals(share, userShare.share) && Objects.equals(amount, userShare.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, share, amount);
    }
}
