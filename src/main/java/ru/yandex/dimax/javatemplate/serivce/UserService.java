package ru.yandex.dimax.javatemplate.serivce;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.yandex.dimax.javatemplate.aop.exectime.LogExecutionTime;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.UserInfoDto;
import ru.yandex.dimax.javatemplate.dto.UserSharesDto;
import ru.yandex.dimax.javatemplate.exception.EntityNotFoundException;
import ru.yandex.dimax.javatemplate.mapper.UserMapper;
import ru.yandex.dimax.javatemplate.model.Gender;
import ru.yandex.dimax.javatemplate.model.Share;
import ru.yandex.dimax.javatemplate.model.User;
import ru.yandex.dimax.javatemplate.repository.ShareRepository;
import ru.yandex.dimax.javatemplate.repository.UserRepository;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private static final String MODEL_NAME = "User";

    private final UserRepository userRepository;
    private final ShareRepository shareRepository;
    private final UserMapper userMapper;

    @LogExecutionTime
    public UserInfoDto getUserById(UUID userId) {
        log.info("Getting %s entity by id=%s".formatted(MODEL_NAME, userId));

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("%s entity with id=%s not found"
                        .formatted(MODEL_NAME, userId))
                );

        log.info("Successfully retrieved %s entity by id=%s".formatted(MODEL_NAME, userId));

        UserInfoDto userDto = userMapper.toUserInfoDto(user);

        return userDto;
    }

    @LogExecutionTime
    public UserInfoDto deleteUser(UUID id) {
        return null;
    }

    @LogExecutionTime
    public UserInfoDto createUser(RegisterUserDto registerUserDto) {
        log.info("Creating a new user");

        User user = userRepository.save(
                userMapper.toEntity(registerUserDto)
        );

        log.info("Successfully created new user with id=%s".format(String.valueOf(user.getId())));

        return userMapper.toUserInfoDto(user);
    }

    @LogExecutionTime
    public UserInfoDto updateUser(UserInfoDto userDto) {
        return null;
    }

    public UserSharesDto buyShare(UUID userId, String shareIsin, Integer shareNumber) {
        log.info("Getting share with isin=%s from database".formatted(shareIsin));

        // вызов в БД, проверка что акции с таким ISIN нет
        // если акция есть, то получаем ее из БД и добавляем юзеру
        // если акции нет, то вызываем MoexApiClient и получаем акции и добавляем ее в БД и нашему юзеру
        /*Optional<Share> shareToBuy = shareRepository.findByIsin(shareIsin);

        if (!shareToBuy.isPresent()) {
            log.info("Share with isin=%s not found in a database. Calling MOEX API");
        }

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("%s entity with id=%s not found"
                        .formatted(MODEL_NAME, userId))
                );

        user.addShare(shareToBuy.get());*/

        Share share = Share.builder()
                .name("Sber")
                .isin("SBER")
                .currentPrice(BigDecimal.TEN)
                .dividends(BigDecimal.ZERO)
                .market("MOEX")
                .build();

        User user = User.builder()
                .name("Dima")
                .age(15)
                .email("dima@gmail.com")
                .password("password")
                .gender(Gender.MALE)
                .userShares(new HashSet<>())
                .build();

        Integer shareAmount = user.getShareAmount(share);

        if (shareAmount == 0) {
            user.addShare(share, shareNumber);
        } else {
            user.addShare(share, shareAmount+shareNumber);
        }

        return userMapper.toUserSharesDto(user);
    }
}
